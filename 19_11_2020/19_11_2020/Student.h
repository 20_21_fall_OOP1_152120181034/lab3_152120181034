#pragma once

#include <string>
using namespace std;


	class Student
	{
	public:
		void setStudent(string _name, int _id);
		void print();
		double calculateGrade();
		void setexams(int a, int b);

	private:
		string name;
		int id;
		int midTermExam=0;
		int finalExam=0;
	};

