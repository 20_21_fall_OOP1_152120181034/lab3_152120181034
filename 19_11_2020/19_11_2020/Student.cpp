#include "Student.h"
#include <iostream>
using namespace std;
void Student::setStudent(string _name, int _id)
{
	name = _name;
	id = _id;
}

void Student::setexams(int a, int b)
{
	midTermExam = a;
	finalExam = b;
}

double Student::calculateGrade() {
	return (midTermExam + finalExam) / 2;
}

void Student::print()
{

	cout << "Name : " << name << endl;
	cout << "ID   : " << id << endl;
	cout << "Grade: " << calculateGrade() << endl;

}
